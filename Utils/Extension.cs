﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Terraria.GameContent.Animations.Actions.NPCs;
using Terraria.ModLoader;
using Terraria;

namespace bettergame.Utils
{
    public static class Extension
    {

        /// <summary>
        /// 判定物品是否可以堆叠至物品堆里，并且堆叠进去
        /// </summary>
        /// <param name="Source"></param>
        /// <param name="items"></param>
        /// <returns></returns>
        public static bool HasStackToSlots(this Item Source, Item[] items)
        {
            int index = 0;
            int lastIndex = -1;
            bool canStack = false;
            while (index < items.Length)
            {
                if (lastIndex == -1 && items[index].IsAir) lastIndex = index;
                if (items[index] is not null && !items[index].IsAir && items[index].type == Source.type)
                {
                    canStack = true;
                    ItemLoader.TryStackItems(items[index], Source, out _);
                    if (Source.IsAir) return true;
                }
                index++;
            }
            return canStack;
        }

        /// <summary>
        /// 堆叠物品到物品堆里
        /// </summary>
        /// <param name="Source"></param>
        /// <param name="items"></param>
        public static void StackToSlots(this Item Source, Item[] items)
        {
            int index = 0;
            int lastIndex = -1;
            while (index < items.Length)
            {
                if (lastIndex == -1 && items[index].IsAir) lastIndex = index; // 记录空槽位
                if (items[index] is not null && !items[index].IsAir && items[index].type == Source.type)
                {
                    ItemLoader.TryStackItems(items[index], Source, out _);
                    if (Source.IsAir) return;
                }
                index++;
            }
            if (lastIndex != -1)
            {
                items[lastIndex] = Source.Clone();
                Source.TurnToAir();
            }
        }

        /// <summary>
        /// 从玩家背包中取出物品并堆叠
        /// </summary>
        /// <param name="items"></param>
        public static void StackFromInventory(Item[] items)
        {
            Player player = Main.LocalPlayer;
            for (int i = 10; i < 50; i++)
            {
                if (!player.inventory[i].IsAir && !player.inventory[i].favorited)
                {
                    for (int j = 0; j < items.Length; j++)
                    {
                        if (CanPlaceInSlot(items[j], player.inventory[i]) is 2)
                        {
                            ItemLoader.TryStackItems(items[j], player.inventory[i], out _);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 强制性的可否放置判断，只有满足条件才能放置物品，没有例外
        /// </summary>
        /// <param name="slotItem">槽内物品</param>
        /// <param name="mouseItem">手持物品</param>
        /// <returns>
        /// 强制判断返回值，判断放物类型
        /// 0: 不可放物<br/>
        /// 1: 两物品不同，应该切换<br/>
        /// 2: 两物品相同，应该堆叠<br/>
        /// 3: 槽内物品为空，应该切换<br/>
        /// </returns>
        public static byte CanPlaceInSlot(Item slotItem, Item mouseItem)
        {
            if (slotItem.IsAir)
                return 3;
            if (mouseItem.type != slotItem.type || mouseItem.prefix != slotItem.prefix)
                return 1;
            if (slotItem.stack < slotItem.maxStack && ItemLoader.CanStack(slotItem, mouseItem))
                return 2;
            return 0;
        }

        /// <summary>
        /// 交换两个物品
        /// </summary>
        /// <param name="item1"></param>
        /// <param name="item2"></param>
        public static void SwapItem(ref Item item1, ref Item item2)
        {
            (item1, item2) = (item2, item1);
        }

        /// <summary>
        /// 从玩家背包中拿取物品
        /// </summary>
        /// <param name="items"></param>
        public static void StorageFromInventory(Item[] items)
        {
            Player player = Main.LocalPlayer;
            for (int i = 10; i < 50; i++)
            {
                if (!player.inventory[i].IsAir && !player.inventory[i].favorited)
                {
                    int index = 0;
                    int lastIndex = -1;
                    while (index < items.Length && !player.inventory[i].IsAir)
                    {
                        byte place = CanPlaceInSlot(items[index], player.inventory[i]);
                        if (lastIndex == -1 && place is 3)
                        {
                            lastIndex = index;
                        }
                        if (place is 2)
                        {
                            ItemLoader.TryStackItems(items[index], player.inventory[i], out _);
                        }
                        index++;
                    }
                    if (lastIndex != -1) SwapItem(ref items[lastIndex], ref player.inventory[i]);
                }
            }
        }

        /// <summary>
        /// 强夺物品到玩家背包中
        /// </summary>
        /// <param name="items"></param>
        public static void CaptureToInventory(Item[] items)
        {
            Player player = Main.LocalPlayer;
            for (int i = 0; i < items.Length; i++)
            {
                if (!items[i].IsAir && !items[i].favorited)
                {
                    int index = 0;
                    int lastIndex = -1;
                    while (index < 50 && !items[i].IsAir)
                    {
                        byte place = CanPlaceInSlot(player.inventory[index], items[i]);
                        if (lastIndex == -1 && place is 3)
                        {
                            lastIndex = index;
                        }
                        if (place is 2)
                        {
                            ItemLoader.TryStackItems(player.inventory[index], items[i], out _);
                        }
                        index++;
                    }
                    if (lastIndex != -1) SwapItem(ref player.inventory[lastIndex], ref items[i]);
                }
            }
        }

        /// <summary>
        /// 排序物品
        /// </summary>
        /// <param name="SlotItems"></param>
        public static void SlotSort(Item[] SlotItems)
        {
            // 取出所有待排序的物品
            List<Item> items = [];
            for (int i = 0; i < SlotItems.Length; i++)
            {
                if (SlotItems[i].IsAir || SlotItems[i].favorited) continue;
                items.Add(SlotItems[i]);
            }
            items.Sort((ItemX, ItemY) => -ItemX.rare.CompareTo(ItemY.rare) * 100 + ItemX.type.CompareTo(ItemY.type) * 10 - ItemX.stack.CompareTo(ItemY.stack));
            int index = 0;
            int insertIndex = 0;
            while (index < SlotItems.Length)
            {
                // 放置物品
                if (insertIndex < items.Count)
                {
                    if (insertIndex < items.Count - 1)
                    {
                        byte place = CanPlaceInSlot(items[insertIndex], items[insertIndex + 1]);
                        if (place is 2)
                        {
                            ItemLoader.TryStackItems(items[insertIndex], items[insertIndex + 1], out _);
                            if (items[insertIndex].stack == items[insertIndex].maxStack)
                            {
                                SlotItems[index] = items[insertIndex];
                                index++;
                            }
                            else
                            {
                                items[insertIndex + 1] = items[insertIndex];
                            }
                        }
                        else if (place is 1 || place is 0)
                        {
                            SlotItems[index] = items[insertIndex];
                            index++;
                        }
                    }
                    else if (!items[insertIndex].IsAir)
                    {

                        SlotItems[index] = items[insertIndex];
                        index++;
                    }
                    insertIndex++;
                }
                else
                {
                    // 放置空物品
                    SlotItems[index] = new Item();
                    index++;
                }
            }
        }
    }
}
