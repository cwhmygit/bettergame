﻿using Terraria;
using Terraria.UI;
using Microsoft.Xna.Framework;
using Terraria.GameContent; //引入TextureASSet类
using Terraria.GameContent.UI.Elements;
using Microsoft.Xna.Framework.Graphics;
using Terraria.ModLoader;
using Terraria.ModLoader.UI.Elements;
using Terraria.ID;
using System.Collections.Generic;
using bettergame.UI.Common;
using bettergame.UI.AutoTrash;
using Terraria.ModLoader.UI;
using Newtonsoft.Json.Bson;

namespace bettergame.Modules.AutoTrash
{
    public class AutoTrashUIState : UIState
    {
        private static bool ChestMenuExists => (Main.LocalPlayer.chest != -1 || Main.npcShop > 0) && !Main.recBigList;
        /// <summary>
        /// 开启 Chest 的时候原版垃圾桶 UI 的 Top 位置
        /// </summary>
        public static readonly float OpenedChestTrashTop = 426f;
        /// <summary>
        /// 开启 Chest 的时候原版垃圾桶 UI 的大小
        /// </summary>
        public static readonly Vector2 OpenedChestTrashSize = new(39f);
        /// <summary>
        /// 开启 Chest 的时候原版垃圾桶 UI 的间距
        /// </summary>
        public static readonly Vector2 OpenedChestTrashSpacing = new(3f);

        /// <summary>
        /// 关闭 Chest 的时候原版垃圾桶 UI 的 Top 位置
        /// </summary>
        public static readonly float ClosedChestTrashTop = 258f;
        /// <summary>
        /// 关闭 Chest 的时候原版垃圾桶 UI 的大小
        /// </summary>
        public static readonly Vector2 ClosedChestTrashSize = new(44f);
        /// <summary>
        /// 关闭 Chest 的时候原版垃圾桶 UI 的间距
        /// </summary>
        public static readonly Vector2 ClosedChestTrashSpacing = new(4f);

        public AutoTrashIcon AutoTrashEdit { get; set; }

        public TrashSlot TrashSlot { get; set; }

        public AutoTrashGrid AutoTrashGrid { get; set; }

        public bool GridVisible
        {
            get => AutoTrashGrid.Parent == this;
            set
            {
                if (value)
                {
                    if (AutoTrashGrid.Parent != null) return;
                    Append(AutoTrashGrid);
                    AutoTrashGrid.Activate();
                }
                else
                {
                    if (AutoTrashGrid.Parent == null) return;

                    RemoveChild(AutoTrashGrid);
                }
            }
        }

        public override void OnInitialize()
        {
            AutoTrashEdit = new(ModContent.Request<Texture2D>("bettergame/Assets/Images/AutoTrashEdit"), Color.White);
            AutoTrashEdit.Width.Set(28, 0);
            AutoTrashEdit.Height.Set(28, 0);
            AutoTrashEdit.Left.Set(363, 0);
            AutoTrashEdit.Top.Set(268, 0);
            AutoTrashEdit.OnLeftClick += AutoTrashEdit_OnLeftClick;

            TrashSlot = new(new Item(), new Color(94, 196, 255, 0.5f));
            TrashSlot.Width.Set(46, 0);
            TrashSlot.Height.Set(46, 0);
            TrashSlot.Left.Set(400, 0);
            TrashSlot.Top.Set(258, 0);

            AutoTrashGrid = new();
            AutoTrashGrid.Height.Set(400f, 0);
            AutoTrashGrid.Left.Set(193f, 0);
            AutoTrashGrid.Top.Set(310f, 0);


            Append(AutoTrashEdit);
            Append(TrashSlot);
        }

        private void AutoTrashEdit_OnLeftClick(UIMouseEvent evt, UIElement listeningElement)
        {
            AutoTrashPlayer.UISystem.UIState.GridVisible = !AutoTrashPlayer.UISystem.UIState.GridVisible; 
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (ChestMenuExists)
            {
                TrashSlot.Top.Set(OpenedChestTrashTop, 0);
                TrashSlot.Left.Set(411, 0);
                TrashSlot.Width.Set(OpenedChestTrashSize.X, 0);
                TrashSlot.Height.Set(OpenedChestTrashSize.Y, 0);
                AutoTrashEdit.Top.Set(OpenedChestTrashTop + 5, 0);
                AutoTrashEdit.Left.Set(379, 0);
            }
            else
            {
                TrashSlot.Top.Set(ClosedChestTrashTop, 0);
                TrashSlot.Left.Set(400, 0);
                TrashSlot.Width.Set(ClosedChestTrashSize.X, 0);
                TrashSlot.Height.Set(ClosedChestTrashSize.Y, 0);
                AutoTrashEdit.Top.Set(ClosedChestTrashTop + 10, 0);
                AutoTrashEdit.Left.Set(363, 0);
            }
            base.Draw(spriteBatch);
        }
    }
}
