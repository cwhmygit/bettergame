﻿using bettergame.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;

namespace bettergame.Modules.AutoTrash
{
    public class AutoTrashUISystem : UISystem
    {
        public new AutoTrashUIState UIState
        {
            get => base.UIState as AutoTrashUIState;
            set => base.UIState = value;
        }

        public override void BeforeUpdateUI()
        {
            if (Main.playerInventory) ShowUI();
            else HideUI();
        }

        public override void UpdatedUI()
        {
            if (UiHelper.MouseInPanel(UIState.AutoTrashEdit.GetDimensions().ToRectangle()) || UiHelper.MouseInPanel(UIState.TrashSlot.GetDimensions().ToRectangle()) || UiHelper.MouseInPanel(UIState.AutoTrashGrid.GetDimensions().ToRectangle()))
            {
                Main.LocalPlayer.mouseInterface = true;
            }
        }
    }
}
