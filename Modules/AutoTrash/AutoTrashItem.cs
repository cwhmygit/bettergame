﻿using bettergame.Modules.AutoTrash;
using bettergame.Modules.BigPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading.Tasks;
using Terraria.Audio;
using Terraria.ID;
using Terraria;
using Terraria.ModLoader;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Terraria.UI;
using Terraria.GameInput;
using Terraria.UI.Gamepad;
using tModPorter;
using bettergame.Utils;
using bettergame.Configs;
namespace bettergame.Modules.AutoTrash
{
    internal class AutoTrashItem : GlobalItem
    {
        public override void Load()
        {
            On_Player.GetItem += On_Player_GetItem;
        }

        /// <summary>
        /// 修改物品拾取的速度
        /// </summary>
        /// <param name="item"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        public override bool GrabStyle(Item item, Player player)
        {
            Vector2 Direction = player.Center - item.position;
            Direction.Normalize();
            item.position += Direction * 15;
            return true;
        }

        /// <summary>
        /// 修改物品拾取的范围
        /// </summary>
        /// <param name="item"></param>
        /// <param name="player"></param>
        /// <param name="grabRange"></param>
        public override void GrabRange(Item item, Player player, ref int grabRange)
        {
            //grabRange = 500 * ModContent.GetInstance<MainConfig>().GrabRange | 32;
        }

        /// <summary>
        /// 添加背包溢出大背包拾取物品
        /// </summary>
        /// <param name="orig"></param>
        /// <param name="self"></param>
        /// <param name="plr"></param>
        /// <param name="source"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        private Item On_Player_GetItem(On_Player.orig_GetItem orig, Player self, int plr, Item source, GetItemSettings settings)
        {
            source = orig.Invoke(self, plr, source, settings);

            if (source.IsAir)
            {
                return source;
            }

            if (settings.LongText == false && settings.NoText == false && settings.CanGoIntoVoidVault == true)
            {
                Item cloneItem = source.Clone();

                // 背包溢出堆叠至其他容器
                if (!source.IsACoin)
                {
                    // 大背包
                    //if (BigPackPlayer.Enable && BigPackPlayer.TryGet(self).CanPickUp)
                    //{
                    //    source.StackToSlots(BigPackPlayer.TryGet(self).CommonItems);
                    //}
                    if (source.IsAir) PickupPopupText(cloneItem, source);
                }
            }

            return source;
        }

        /// <summary>
        /// 是否可以拾取物品
        /// </summary>
        /// <param name="item"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        public override bool OnPickup(Item item, Player player)
        {
            //if (item.questItem)
            //{
            //    item.uniqueStack = false;
            //    item.maxStack = Item.CommonMaxStack;
            //    return true;
            //}
            if (item.IsACoin) return true;
            if (ModContent.GetInstance<MainConfig>().AutoTrash)
            {
                bool isTrash = AutoTrashPlayer.LocalPlayer.IsTrash(item.type);
                if (isTrash) return false;
            }
            //if (BigPackPlayer.Enable && BigPackPlayer.TryGet(Main.LocalPlayer).CanPickUp && item.HasStackToSlots(BigPackPlayer.TryGet(Main.LocalPlayer).CommonItems))
            //{
            //    Item cloneItem = item.Clone();
            //    PickupPopupText(cloneItem, item);
            //}
            return true;
        }

        /// <summary>
        /// 背包溢出时是否可以拾取
        /// </summary>
        /// <param name="item"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        public override bool ItemSpace(Item item, Player player)
        {
            if (item.IsACoin) return false;
            //if (BigPackPlayer.Enable && BigPackPlayer.TryGet(Main.LocalPlayer).CanPickUp) return true;
            return false;
        }

        /// <summary>
        /// 物品拾取后提示
        /// </summary>
        private static void PickupPopupText(Item cloneItem, Item self)
        {
            if (self.stack < cloneItem.stack)
            {
                SoundEngine.PlaySound(SoundID.Grab);
                PopupText.NewText(PopupTextContext.ItemPickupToVoidContainer, cloneItem, cloneItem.stack - self.stack);
            }
        }

        /// <summary>
        /// 开启多线钓鱼
        /// </summary>
        /// <param name="item"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        public override bool CanShoot(Item item, Player player)
        {
            // done 多线吊钩
            //if (ModContent.GetInstance<MainConfig>().MutiFishLineNumber > 0 && FisherData.FishingPole.ContainsKey(item.type))
            //{
            //    Vector2 direction = Main.MouseWorld - player.position;
            //    direction.Normalize();

            //    direction *= item.shootSpeed;
            //    int shootType = player.overrideFishingBobber == -1 ? item.shoot : player.overrideFishingBobber;
            //    for (int i = 0; i < ModContent.GetInstance<MainConfig>().MutiFishLineNumber; i++)
            //    {
            //        Vector2 randMouse = direction + new Vector2(Main.rand.Next(-5, 5), 0);
            //        var entitySource = Item.GetSource_NaturalSpawn();
            //        Projectile.NewProjectile(entitySource, player.position, randMouse, shootType, 0, 0, player.whoAmI);
            //    }
            //}
            return true;
        }

        /// <summary>
        /// 添加召唤物剩余提示
        /// </summary>
        /// <param name="item"></param>
        /// <param name="tooltips"></param>
        public override void ModifyTooltips(Item item, List<TooltipLine> tooltips)
        {

            Player player = Main.LocalPlayer;

            // done 添加当前已召唤物数量和可以召唤的总数提示语句
            if (item.DamageType == DamageClass.Summon)
            {
                // 获取玩家的总召唤数量
                int maxMinions = player.maxMinions;
                int currentMinion = (int)player.slotsMinions;
                string toolText = currentMinion.ToString() + "/" + maxMinions.ToString() + "当前召唤数量";
                TooltipLine tooltipLine = new(Bettergame.Mod, "ShowMinions", toolText)
                {
                    OverrideColor = currentMinion < maxMinions ? Color.Green : Color.Red
                };
                tooltips.Add(tooltipLine);
            }
        }
    }
}
