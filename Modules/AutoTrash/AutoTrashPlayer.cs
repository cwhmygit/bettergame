﻿using bettergame.Modules.BigPack;
using bettergame.UI.BigPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using Terraria.ModLoader.IO;

namespace bettergame.Modules.AutoTrash
{
    internal class AutoTrashPlayer : ModPlayer
    {
        /// <summary>
        /// 垃圾物品类型集合
        /// </summary>
        public HashSet<int> TrashTypes = [];

        public static AutoTrashPlayer LocalPlayer => Main.LocalPlayer.GetModPlayer<AutoTrashPlayer>();

        public static AutoTrashUISystem UISystem => ModContent.GetInstance<AutoTrashUISystem>();

        public override void LoadData(TagCompound tag)
        {
            TrashTypes = tag.Get<List<int>>("MyTrashItems").Where(type => type < ItemID.Count).ToHashSet();
        }

        public override void SaveData(TagCompound tag)
        {
            tag["MyTrashItems"] = TrashTypes.ToList();
        }

        /// <summary>
        /// 初始化UI
        /// </summary>
        public override void OnEnterWorld()
        {
            UISystem.LoadUI("Better: auto trash", new AutoTrashUIState());
            base.OnEnterWorld();
        }

        public bool IsTrash(int type)
        {
            return TrashTypes.Contains(type);
        }

        /// <summary>
        /// 添加物品至列表
        /// </summary>
        /// <param name="ItemType"></param>
        /// <returns></returns>
        public void TryAddTrash(int ItemType)
        {
            if (TrashTypes.Contains(ItemType) || ItemType == 0) return;
            // 添加槽
            TrashTypes.Add(ItemType);
            UISystem.UIState.AutoTrashGrid.ReDraw();
        }

        /// <summary>
        /// 删除列表中的物品
        /// </summary>
        /// <param name="ItemType"></param>
        /// <returns></returns>
        public void TryRemoveTrash(int ItemType)
        {
            if(TrashTypes.Remove(ItemType))
            {
                UISystem.UIState.AutoTrashGrid.ReDraw();
            }
        }

        /// <summary>
        /// 清空垃圾桶列表
        /// </summary>
        public void RemoveAll()
        {
            TrashTypes.Clear();
            UISystem.UIState.AutoTrashGrid.ReDraw();
        }

        /// <summary>
        /// 快速清空背包中的垃圾
        /// </summary>
        public void InventoryQuickTrash()
        {
            Player player = Main.LocalPlayer;
            for (int i = 0; i < player.inventory.Length; i++)
            {
                if (player.inventory[i].IsAir || player.inventory[i].favorited) continue;
                if (TrashTypes.Contains(player.inventory[i].type))
                {
                    player.inventory[i] = new Item();
                }
            }
            // 同时清空大背包中的垃圾
            //if (BigPackPlayer.TryGet(Main.LocalPlayer).CanAutoTrash)
            //{
            //    SlotItem[] items = BigPackPlayer.TryGet(Main.LocalPlayer).CommonItems;
            //    for (int i = 0; i < items.Length; i++)
            //    {
            //        if (items[i].Item.IsAir || items[i].Item.favorited) continue;
            //        if (TrashTypes.Contains(items[i].Item.type))
            //        {
            //            items[i].Item = new Item();
            //        }
            //    }
            //}
        }
    }
}
