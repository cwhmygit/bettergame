﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria.Audio;
using Terraria.ID;
using Terraria;
using Terraria.UI;
using bettergame.UI;

namespace bettergame.Modules.BigPack
{
    public class BigPackUISystem : UISystem
    {
        public new BigPackUIState UIState
        {
            get => base.UIState as BigPackUIState;
            set => base.UIState = value;
        }

        public override void ShowUI()
        {
            //if (!Visible) SoundEngine.PlaySound(SoundID.MenuOpen);
            //if (!Main.playerInventory) Main.playerInventory = true;
            base.ShowUI();
        }
        public override void HideUI()
        {
            if (Visible) SoundEngine.PlaySound(SoundID.MenuClose);
            base.HideUI();
        }
        public override void BeforeUpdateUI()
        {
            //if (!Main.playerInventory) HideUI();
            base.BeforeUpdateUI();
        }

        public override void UpdatedUI()
        {
            if (UiHelper.MouseInPanel(UIState.BigPackGrid.GetDimensions().ToRectangle()))
            {
                Main.LocalPlayer.mouseInterface = true;
            }
        }
    }
}
