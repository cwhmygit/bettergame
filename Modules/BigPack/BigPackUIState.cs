﻿using bettergame.UI.Common;
using Terraria;
using Terraria.GameContent.UI.Elements;
using Terraria.UI;
using Microsoft.Xna.Framework;
using bettergame.UI.BigPack;
namespace bettergame.Modules.BigPack
{
    public class BigPackUIState : UIState
    {
        public BigPackGrid BigPackGrid {  get; set; }

        public override void OnInitialize()
        {
            BigPackGrid = new BigPackGrid();
            BigPackGrid.Height.Set(500f, 0);
            BigPackGrid.Left.Set(600f, 0);
            BigPackGrid.Top.Set(200f, 0);

            Append(BigPackGrid);

            base.OnInitialize();
        }
    }
}
