﻿using bettergame.Configs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ModLoader;
using static bettergame.Modules.BigPack.BigPackPlayer;
using Terraria.ModLoader.IO;
using bettergame.UI;
using bettergame.Utils;

namespace bettergame.Modules.BigPack
{
    public class BigPackPlayer : ModPlayer
    {
        public static BigPackPlayer LocalPlayer => Main.LocalPlayer.GetModPlayer<BigPackPlayer>();

        public static BigPackUISystem BigPackUISystem => ModContent.GetInstance<BigPackUISystem>();

        /// <summary>
        /// 普通物品的数量上限
        /// </summary>
        public static int CommonMaxSize => 150;
        /// <summary>
        /// 收藏的物品数量上限
        /// </summary>
        public static int FavoritedMaxSize => 50;
        /// <summary>
        /// 普通物品
        /// </summary>
        public Item[] CommonItems = new Item[CommonMaxSize];
        /// <summary>
        /// 收藏物品
        /// </summary>
        public Item[] FavoriteItems = new Item[FavoritedMaxSize];
        /// <summary>
        /// 是否开启大背包功能
        /// </summary>
        public static bool Enable => ModContent.GetInstance<MainConfig>().BigBackPack;
        /// <summary>
        /// 分类 0：表示普通 1：表示收藏
        /// </summary>
        private byte _column = 0;
        public byte Column
        {
            get
            {
                return _column;
            }
            set
            {
                if (value == _column) return;
                _column = value;
                OnColumnChanged?.Invoke(_column);
                BigPackUISystem.UIState.BigPackGrid.ReDraw();
                // 重绘SlotGrid
                // 部分功能根据分类判断
            }
        }

        public Item[] GetItems => _column is 0 ? CommonItems : FavoriteItems;

        public delegate void ColumnChange(byte column);
        /// <summary>
        /// 分类改变事件
        /// </summary>
        public event ColumnChange OnColumnChanged;
        /// <summary>
        /// 是否开启自动拾取
        /// </summary>
        public bool CanPickUp { get; set; } = false;
        /// <summary>
        /// 是否开启垃圾清理
        /// </summary>
        public bool CanAutoTrash { get; set; } = false;
        /// <summary>
        /// 参与合成
        /// </summary>
        public bool CanFindRecipe { get; set; } = false;

        public override void OnEnterWorld()
        {
            BigPackUISystem.LoadUI("Better: big pack", new BigPackUIState());
            BigPackUISystem.ShowUI();
            //OnColumnChanged += ColumnChanged;
            //Column = 0;
            base.OnEnterWorld();
        }

        public override void LoadData(TagCompound tag)
        {
            Item[] commonItems = tag.Get<Item[]>("BigPackItems");
            Item[] favoriteItems = tag.Get<Item[]>("FavoriteItems");
            for (int i = 0; i < CommonMaxSize; i++)
            {
                CommonItems[i] = commonItems[i] ?? new Item();
            }
            for (int i = 0; i < FavoritedMaxSize; i++)
            {
                FavoriteItems[i] = favoriteItems[i] ?? new Item();
            }
        }
        public override void SaveData(TagCompound tag)
        {
            tag["BigPackItems"] = CommonItems.Select(item => item ?? new Item()).ToList();
            tag["FavoriteItems"] = FavoriteItems.Select(item => item ?? new Item()).ToList();
        }


        // 存放全部
        public void Storage()
        {
            if (Column is 0)
            {
                Extension.StorageFromInventory(CommonItems);
                BigPackUISystem.UIState.BigPackGrid.ReDraw();
                Recipe.FindRecipes();
            }
        }
    }
}
