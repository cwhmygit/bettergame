using System.ComponentModel;
using Terraria.ID;
using Terraria.ModLoader.Config;

namespace bettergame.Configs
{
	// [BackgroundColor(99, 180, 209)]
	public class MainConfig : ModConfig
	{
		// ConfigScope.ClientSide should be used for client side, usually visual or audio tweaks.
		// ConfigScope.ServerSide should be used for basically everything else, including disabling items or changing NPC behaviours
		public override ConfigScope Mode => ConfigScope.ClientSide;


        [Header("物品设置")] // Headers are like titles in a config. You only need to declare a header on the item it should appear over, not every item in the category.


        [DefaultValue(false)]

        public bool TileIncrease;
		//      // The "" character before a name means it should interpret the name as a translation key and use the loaded translation with the same key.
		//      // The things in brackets are known as "Attributes".
		//      [Label nn("$设置工具使用速度")] // A LabelKey is the text displayed next to the option. This should usually be a short description of what it does.
		//[SliderColor(255, 0, 127)]
		//[Increment(0.1f)]
		//// [TooltipKey("加快工具使用速度")] // A TooltipKey is a description showed when you hover your mouse over the option. It can be used as a more in-depth explanation of the option.
		//[DefaultValue(0f)] // This sets the configs default value.
		//// [ReloadRequired] // Marking it with [ReloadRequired] makes tModLoader force a mod reload if the option is changed. It should be used for things like item toggles, which only take effect during mod loading
		//public float PickFast; // To see the implementation of this option, see ExampleWings.cs


		//[LabelKey("$开启召唤物无消耗")] 
		//[TooltipKey("$开启后使用召唤物将无消耗")] 
		//[DefaultValue(false)] 

		//public bool InfiniteBossSummon;


		//[LabelKey("$开启药水无消耗")] 
		//[TooltipKey("$开启后使用药水将无消耗")] 
		//[DefaultValue(false)] 

		//public bool InfinitePotion;



		//[LabelKey("$设置药水无消耗生效条件")]
		//[TooltipKey("$设置开启无限药水效果至少所需药水的数量")] 
		//[Range(10f, 30f)]
		//[Increment(5f)]
		//[DrawTicks]
		//[DefaultValue(10f)]
		//public float InfinitePotionNumber;



		//[LabelKey("$开启弹药无消耗")] 
		//[TooltipKey("$背包中每种子弹数量至少为3999才生效")] 
		//[DefaultValue(false)] 

		//public bool InfiniteAmmo;
		///// //////////////////////////////////////////////////////////////////////////////////////////////

		//      [Header("NPC设置")]


		//[LabelKey("$旅商永不离开")] 
		//[DefaultValue(false)] 

		//public bool TravelStay;


		//[LabelKey("$开启满血复活")] 
		//[DefaultValue(false)] 

		//public bool MaxHealRespawn;


		//[LabelKey("$加快非Boss战复活时间")]
		//[Increment(0.1f)]
		//[SliderColor(255, 0, 127)]
		//[DefaultValue(0f)]
		//public float RespawnTimeRate;

		//[LabelKey("$加快Boss战复活时间")]
		//[Increment(0.1f)]
		//[SliderColor(255, 0, 127)]
		//[DefaultValue(0f)]
		//public float BossRespawnTimeRate;


		//[LabelKey("$设置掉落物拾取距离")]
		//[TooltipKey("$默认为0，最大距离为500")]
		//[Range(0,500)]
		//[Increment(1f)]
		//[SliderColor(255, 0, 127)]
		//public int GrabRange;


		//[LabelKey("$提高敌怪掉落钱币倍率")] 
		//[DefaultValue(false)] 
		//public bool MultiDropCoins;

		//      [LabelKey("$开启渔夫任务无冷却")] 
		//[TooltipKey("$开启后自动刷新渔夫任务")] 
		//[DefaultValue(false)]

		//      public bool InfinitAnglerFish;

		///// ///////////////////////////////////////////////////////////////////////////////////////
		///// 
		//      [Header("游戏机制设置")]

		//      [LabelKey("$禁止墓碑生成")]
		//      [DefaultValue(false)]

		//      public bool NoTombStone;

		//      [LabelKey("$提高敌怪刷新速率")]
		//      [TooltipKey("$最多增加10倍刷新速率")]
		//      [Range(0, 10f)]
		//      [SliderColor(255, 0, 127)]
		//      [Increment(1f)]
		//      [DrawTicks]
		//      [DefaultValue(0)]
		//      public float NPCSpawnRate;

		//      [LabelKey("$开启自动钓鱼")] 
		//[DefaultValue(false)] 

		//public bool AutoFish;

		//[LabelKey("$设置钓鱼提线时延")]
		//[Range(0,1f)]
		//[Increment(0.1f)]
		//[SliderColor(255, 0, 127)]
		//public float FishingDelay;

		//[LabelKey("$只钓任务鱼")] 
		//[DefaultValue(false)] 

		//public bool OnlyQuestFish;

		//[LabelKey("$只钓宝匣")] 
		//[DefaultValue(false)] 

		//public bool OnlyCrates;

		//[LabelKey("$只钓罕见以上的物品")] 
		//[DefaultValue(false)] 

		//public bool OnlyUpRare;

		//[LabelKey("$不钓事件敌怪")] 
		//[DefaultValue(false)] 

		//public bool NoNPC;

		//[LabelKey("$多线钓鱼")]
		//[TooltipKey("$最多增加50条钩线")]
		//[Range(0, 50)]
		//[Increment(1)]
		//[DefaultValue(0)]
		//public int MutiFishLineNumber;

		[Header("模组辅助设置")]

		[DefaultValue(false)]

		[ReloadRequired]
		public bool AutoTrash;


		[DefaultValue(false)]

		[ReloadRequired]
		public bool BigBackPack;

		//      [LabelKey("$开启智能存储")]
		//      [DefaultValue(false)]

		//      [ReloadRequired]
		//      public bool SmartBox;
		// [LabelKey("开启快捷打开猪猪存钱罐和虚空袋")] 
		// [TooltipKey("开启后使用中键点击快捷开启")]
		// [DefaultValue(false)] 

		// public bool QuickOpenChest;
	}
}
