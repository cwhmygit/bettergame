﻿using bettergame.UI.Common;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ReLogic.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria.UI;
using Terraria.ModLoader;
using Terraria.Audio;
using Terraria.ID;

namespace bettergame.UI.AutoTrash
{
    public class AutoTrashIcon(Asset<Texture2D> asset, Color color) : IconUI(asset, color)
    {
        private bool _isHover { get; set; } = false;

        public override Color Color 
        { 
            get => base.Color * (_isHover ? 1f : 0.7f); 
            set => base.Color = value; 
        }

        public override void MouseOver(UIMouseEvent evt)
        {
            _isHover = true;
            SoundEngine.PlaySound(SoundID.Mech);
            base.MouseOver(evt);
        }

        public override void MouseOut(UIMouseEvent evt)
        {
            _isHover = false;
            base.MouseOut(evt);
        }
    }
}
