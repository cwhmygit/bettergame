﻿#nullable enable
using bettergame.Modules.AutoTrash;
using bettergame.UI.Common;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.GameContent.UI.Elements;
using Terraria.ModLoader;
using Terraria.ModLoader.UI;
using Terraria.UI;

namespace bettergame.UI.AutoTrash
{
    public class AutoTrashGrid(float scrollBarWidth = 20, bool disabled = false, float transparency = 0.7F) : ScrollPanel(null, scrollBarWidth, disabled, transparency)
    {
        public override void OnInitialize()
        {
            Container = new UIPanel
            {
                MarginTop = 75f
            };

            Content = new UIElement();
            Content.Width.Set(236f, 0);
            Content.Height.Set(800f, 0);
            Content.MaxHeight.Set(800f, 0);

            UIText uITitle = new("自动丢弃列表", 1.2f);

            AutoTrashIcon uIClose = new(ModContent.Request<Texture2D>("bettergame/Assets/Images/close"), Color.Red);
            uIClose.Width.Set(24f, 0);
            uIClose.Height.Set(24f, 0);
            uIClose.Left.Set(Content.Width.Pixels + _gap + Container.PaddingLeft + Container.PaddingRight + _scrollBarWidth - 24f, 0);
            uIClose.Top.Set(-3f, 0);
            uIClose.OnLeftClick += UIClose_OnLeftClick;

            TextButton quickTrashBtn = new("快速丢弃",0.8f, padding: 8);
            quickTrashBtn.Top.Set(35f, 0);
            quickTrashBtn.OnLeftClick += QuickTrashBtn_OnLeftClick;

            TextButton clearBtn = new("清空列表", 0.8f, padding: 8);
            clearBtn.Top.Set(35f, 0);
            clearBtn.Left.Set(Content.Width.Pixels - clearBtn.Width.Pixels + _gap + _scrollBarWidth + Container.PaddingLeft + Container.PaddingRight, 0);
            clearBtn.OnLeftClick += ClearBtn_OnLeftClick;

            Append(uITitle);
            Append(uIClose);
            Append(quickTrashBtn);
            Append(clearBtn);
            base.OnInitialize();
        }

        private void ClearBtn_OnLeftClick(UIMouseEvent evt, UIElement listeningElement)
        {
            AutoTrashPlayer.LocalPlayer.RemoveAll(); 
        }

        private void QuickTrashBtn_OnLeftClick(UIMouseEvent evt, UIElement listeningElement)
        {
            AutoTrashPlayer.LocalPlayer.InventoryQuickTrash();
        }

        private void UIClose_OnLeftClick(UIMouseEvent evt, UIElement listeningElement)
        {
            AutoTrashPlayer.UISystem.UIState.GridVisible = false;
        }

        public override void OnActivate()
        {
            ReDraw(); 
            base.OnActivate(); 
        }

        public void ReDraw()
        {
            if (Content == null || Parent == null) return;

            Content.RemoveAllChildren();

            HashSet<int> types = AutoTrashPlayer.LocalPlayer.TrashTypes;

            int column = 0, row = 0;
            foreach (int type in types)
            {
                GridSlot gridSlot = new(new Item(type));
                gridSlot.Width.Set(48f, 0);
                gridSlot.Height.Set(48f, 0);
                gridSlot.Left.Set(column * 58, 0);
                gridSlot.Top.Set(row * 58, 0);

                if (++column > 3)
                {
                    row++;
                    column = 0;
                }
                Content.Append(gridSlot);
            }
        }
    }
}
