﻿using bettergame.Modules.AutoTrash;
using bettergame.UI.Common;
using bettergame.Utils;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.Audio;
using Terraria.ID;
using Terraria.ModLoader;
using Terraria.UI;

namespace bettergame.UI.AutoTrash
{
    public class TrashSlot(Item item, Color? color = null) : SlotItemUI(item, color)
    {
        protected override bool DefaultCursorAction => false;

        public override void LeftMouseDown(UIMouseEvent evt)
        {
            if (Main.mouseItem.IsAir && Item.IsAir) return;

            if(!Main.mouseItem.IsAir && Main.mouseItem != null)
            {
                Item = Main.mouseItem;
                Main.mouseItem = new Item();
                SoundEngine.PlaySound(SoundID.Grab);
                AutoTrashPlayer.LocalPlayer.TryAddTrash(Item.type);
                return;
            }

            base.LeftMouseDown(evt);

            AutoTrashPlayer.LocalPlayer.TryAddTrash(Item.type);
        }
    }
}
