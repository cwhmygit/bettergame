﻿using bettergame.Modules.AutoTrash;
using bettergame.UI.Common;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.UI;

namespace bettergame.UI.AutoTrash
{
    public class GridSlot(Item item, Color? color = null) : SlotUI(item, color)
    {
        public override void LeftMouseDown(UIMouseEvent evt)
        {
            AutoTrashPlayer.LocalPlayer.TryRemoveTrash(Item.type);
            base.LeftMouseDown(evt);
        }
    }
}
