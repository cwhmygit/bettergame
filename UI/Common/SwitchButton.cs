﻿using Terraria;
using Terraria.UI;
using Microsoft.Xna.Framework;
using Terraria.GameContent; //引入TextureASSet类
using Terraria.GameContent.UI.Elements;
using Microsoft.Xna.Framework.Graphics;
using Terraria.ModLoader;
using Terraria.ModLoader.UI.Elements;
using Terraria.ID;
using System.Collections.Generic;
using System;
using Terraria.Audio;

namespace bettergame.UI.Common
{
    /// <summary>
    /// 开关按钮组件
    /// </summary>
    /// <param name="defaultChecked">是否被选中</param>
    /// <param name="duration">动画时间/ms</param>
    public class SwitchButton(bool defaultChecked = false, int duration = 100) : UIPanel
    {
        private readonly IconUI _circle = new(ModContent.Request<Texture2D>("betterexperience/Assets/Images/circle"), new Color(3, 15, 67));

        private readonly int _duration = duration;

        private int _timer = 0;

        private bool _checked = defaultChecked;

        public bool Checked
        {
            get
            {
                return _checked;
            }
            set
            {
                _checked = value;

                if (value)
                {
                    SetAnimation();
                    SetView(new Color(2, 167, 250), Color.LightSkyBlue);
                }
                else
                {
                    SetAnimation(-1);
                    SetView(new Color(3, 15, 67), Color.Black);
                }

                OnCheckedChange?.Invoke(value);
            }
        }


        public delegate void CheckedChanged(bool isChecked);

        /// <summary>
        /// 开关选中改变事件
        /// </summary>
        public event CheckedChanged OnCheckedChange;

        public override void OnInitialize()
        {
            // 设置开关圆点组件
            SetPadding(2);
            PaddingLeft = PaddingRight = 5;
            _circle.Width.Set(15f, 0);
            _circle.Height.Set(15f, 0);
            _circle.Left.Set(0, 0);
            _circle.Top.Set((Height.Pixels - 15f - PaddingTop - PaddingBottom) / 2, 0);

            Append(_circle);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (_timer != 0)
            {
                CalculatedStyle inner = GetInnerDimensions();

                float left = _circle.Left.Pixels + ((inner.Width - 15f) / _duration) * (_timer > 0 ? 1 : -1);

                _circle.Left.Set(left, 0);

                _timer = _timer > 0 ? _timer + 1 : _timer - 1;

                if (Math.Abs(_timer) == _duration)
                {
                    _timer = 0;
                }
            }
            base.Draw(spriteBatch);
        }

        public override void LeftClick(UIMouseEvent evt)
        {
            Checked = !Checked;
            SoundEngine.PlaySound(SoundID.Mech);
            base.LeftClick(evt);
        }

        public virtual void SetView(Color? circleColor, Color? borderColor)
        {
            _circle.Color = circleColor ?? _circle.Color;
            BorderColor = borderColor ?? BorderColor;
        }

        private void SetAnimation(int timer = 1)
        {
            _timer = timer;
        }
    }
}
