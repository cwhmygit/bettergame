﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ReLogic.Content;
using Terraria.GameInput;
using Terraria.UI;
using Terraria;

namespace bettergame.UI.Common
{
    public class Scrollbar : UIElement
    {
        private float _viewPosition;

        private float _viewSize = 1f;

        private float _maxViewSize = 20f;

        private bool _isDragging;

        private bool _isHoveringOverHandle;

        private float _dragYOffset;

        private readonly Asset<Texture2D> _texture = Main.Assets.Request<Texture2D>("Images/UI/Scrollbar");

        private readonly Asset<Texture2D> _innerTexture = Main.Assets.Request<Texture2D>("Images/UI/ScrollbarInner");

        public bool CanScroll => _maxViewSize != _viewSize;

        public float ViewSize => _viewSize;

        public float MaxViewSize => _maxViewSize;

        public bool Visible { get; set; } = true;

        public float ViewPosition
        {
            get
            {
                return _viewPosition;
            }
            set
            {
                _viewPosition = MathHelper.Clamp(value, 0f, _maxViewSize - _viewSize);
                OnViewPositionChanged?.Invoke(_viewPosition / _maxViewSize, _viewPosition, _maxViewSize);
            }
        }

        public void GoToBottom()
        {
            ViewPosition = _maxViewSize - _viewSize;
        }

        public void SetView(float positionRatio)
        {
            ViewPosition = _maxViewSize * positionRatio;
        }

        public void SetView(float positionRatio, float sizeRatio)
        {
            sizeRatio = MathHelper.Clamp(sizeRatio, 0f, 1f);
            _viewSize = _maxViewSize * sizeRatio;
            SetView(positionRatio);
        }

        public delegate void ViewPositioinChanged(float ratio, float value, float maxViewSize);
        /// <summary>
        /// 定义VIewPosition改变事件
        /// </summary>
        public event ViewPositioinChanged? OnViewPositionChanged;

        public Scrollbar(float maxViewSize = 20f)
        {
            _maxViewSize = maxViewSize;
            Width.Set(20f, 0f);
            MaxWidth.Set(20f, 0f);
            SetPadding(0f);
            PaddingTop = PaddingBottom = 6f;
        }

        protected override void DrawSelf(SpriteBatch spriteBatch)
        {
            if (!Visible) return;

            CalculatedStyle dimensions = GetDimensions();
            CalculatedStyle innerDimensions = GetInnerDimensions();
            if (_isDragging)
            {
                float num = Main.mouseY - innerDimensions.Y - _dragYOffset;
                ViewPosition = MathHelper.Clamp(num / innerDimensions.Height * _maxViewSize, 0f, _maxViewSize - _viewSize);
            }

            Rectangle handleRectangle = GetHandleRectangle();
            Vector2 mousePosition = UserInterface.ActiveInstance.MousePosition;
            bool isHoveringOverHandle = _isHoveringOverHandle;
            _isHoveringOverHandle = handleRectangle.Contains(new Point((int)mousePosition.X, (int)mousePosition.Y));
            if (!isHoveringOverHandle && _isHoveringOverHandle && Main.hasFocus)
            {
                //SoundEngine.PlaySound(12);
            }
            DrawBar(spriteBatch, _texture.Value, dimensions.ToRectangle(), Color.White);
            DrawBar(spriteBatch, _innerTexture.Value, handleRectangle, Color.White * ((_isDragging || _isHoveringOverHandle) ? 1f : 0.85f));
        }

        public override void LeftMouseDown(UIMouseEvent evt)
        {
            base.LeftMouseDown(evt);
            if (evt.Target == this)
            {
                Rectangle handleRectangle = GetHandleRectangle();
                if (handleRectangle.Contains(new Point((int)evt.MousePosition.X, (int)evt.MousePosition.Y)))
                {
                    _isDragging = true;
                    _dragYOffset = evt.MousePosition.Y - (float)handleRectangle.Y;
                }
                else
                {
                    CalculatedStyle innerDimensions = GetInnerDimensions();
                    float num = Main.mouseY - innerDimensions.Y - (float)(handleRectangle.Height >> 1);
                    ViewPosition = MathHelper.Clamp(num / innerDimensions.Height * _maxViewSize, 0f, _maxViewSize - _viewSize);
                }
            }
        }

        public override void LeftMouseUp(UIMouseEvent evt)
        {
            base.LeftMouseUp(evt);
            _isDragging = false;
        }

        public override void MouseOver(UIMouseEvent evt)
        {
            base.MouseOver(evt);
            PlayerInput.LockVanillaMouseScroll("ModLoader/Scrollbar");
        }

        internal static void DrawBar(SpriteBatch spriteBatch, Texture2D texture, Rectangle dimensions, Color color)
        {
            spriteBatch.Draw(texture, new Rectangle(dimensions.X, dimensions.Y, dimensions.Width, 6), new Rectangle(0, 0, texture.Width, 6), color);
            spriteBatch.Draw(texture, new Rectangle(dimensions.X, dimensions.Y + 6, dimensions.Width, dimensions.Height- 12), new Rectangle(0, 6, texture.Width, 4), color);
            spriteBatch.Draw(texture, new Rectangle(dimensions.X, dimensions.Y + dimensions.Height - 6, dimensions.Width, 6), new Rectangle(0, texture.Height - 6, texture.Width, 6), color);
        }

        private Rectangle GetHandleRectangle()
        {
            CalculatedStyle innerDimensions = GetInnerDimensions();
            if (_maxViewSize <= 0f && _viewSize <= 0f)
            {
                _viewSize = 20f;
                _maxViewSize = 20f;
            }

            return new Rectangle((int)innerDimensions.X, (int)(innerDimensions.Y + innerDimensions.Height * (_viewPosition / _maxViewSize)), 20, (int)(innerDimensions.Height * (_viewSize / _maxViewSize)));
        }
    }
}
