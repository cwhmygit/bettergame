﻿using Terraria.UI;
using Microsoft.Xna.Framework;
using Terraria.GameContent; //引入TextureASSet类
using Terraria.GameContent.UI.Elements;
using Microsoft.Xna.Framework.Graphics;
using Terraria.Audio;
using Terraria.ID;
using System.Text;
using ReLogic.Graphics;
using Terraria;
using Humanizer;
using System;


namespace bettergame.UI.Common
{
    public class InputButtton(string defaultText, float textScale = 1f) : UIPanel()
    {

        /// <summary>
        /// 是否聚焦
        /// </summary>
        private bool _focus = false;
        /// <summary>
        /// 是否鼠标浮动
        /// </summary>
        private bool _hover = false;
        /// <summary>
        /// 是否播放音效
        /// </summary>
        private bool _sound = false;
        /// <summary>
        /// 是否被选中
        /// </summary>
        private bool _selected = false;
        /// <summary>
        /// 文本颜色
        /// </summary>
        private Color _textColor = Color.White;
        /// <summary>
        /// 计时器
        /// </summary>
        private byte _timer = 0;
        /// <summary>
        /// 文本内容
        /// </summary>
        private string _text = defaultText;

        private readonly float _textScale = textScale;



        public virtual bool Focus
        {
            get { return _focus; }
            set
            {
                _focus = value;
            }
        }

        public virtual bool Hover
        {
            get { return _hover; }
            set
            {
                _hover = value;
                OnHoverChanged?.Invoke(value);
            }
        }

        public virtual bool Sound
        {
            get { return _sound; }
            set
            {
                _sound = value;
            }
        }

        public virtual bool Selected
        {
            get { return _selected; }
            set
            {
                _selected = value;
                OnSelectChanged?.Invoke(value);
            }
        }

        private byte Timer
        {
            get { return _timer; }
            set
            {
                _timer = value;
                if (_timer == 60)
                {
                    _timer = 0;
                }
            }
        }


        public string Text
        {
            get { return _text; }
            set
            {

                string text = FontAssets.DeathText.Value.CreateWrappedText(value, Width.Pixels);
                if (!_text.Equals(value))
                {
                    _text = value;
                    OnTextChanged?.Invoke(_text);
                }
            }
        }

        private string ShowText
        {
            get
            {
                if (!_focus) return _text;
                return _timer <= 30 ? $"{_text}|" : _text;
            }
        }

        public delegate void TextChanged(string text);

        public delegate void HoverChanged(bool isHover);

        public delegate void SelectChanged(bool isSelected);

        public event TextChanged OnTextChanged;

        public event HoverChanged OnHoverChanged;

        public event SelectChanged OnSelectChanged;

        public override void LeftDoubleClick(UIMouseEvent evt)
        {
            Focus = true;

            base.LeftDoubleClick(evt);
        }

        public override void LeftClick(UIMouseEvent evt)
        {
            Selected = true;

            base.LeftClick(evt);
        }

        public override void MouseOver(UIMouseEvent evt)
        {
            if (!Selected) Hover = true;

            base.MouseOver(evt);
        }

        public override void MouseOut(UIMouseEvent evt)
        {

            Focus = false;
            Timer = 0;
            if (!Selected) Hover = false;

            base.MouseOut(evt);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (_hover)
            {
                if (!_sound)
                {
                    SoundEngine.PlaySound(SoundID.Mech);
                    _sound = true;
                }
            }
            else
            {
                _sound = false;
            }

            if (_focus)
            {
                Timer++;
                Terraria.GameInput.PlayerInput.WritingText = true;
                Main.instance.HandleIME();
                Text = Main.GetInputText(_text);
            }
            base.Draw(spriteBatch);

            Rectangle rect = GetDimensions().ToRectangle();
            // 绘制文字
            StringBuilder stringBuilder = new();
            stringBuilder.Append(ShowText);
            spriteBatch.DrawString(FontAssets.DeathText.Value, stringBuilder, new Vector2(rect.X + PaddingLeft, rect.Y + PaddingTop), _textColor, 0, Vector2.Zero, _textScale, SpriteEffects.None, 0);
        }
    }
}
