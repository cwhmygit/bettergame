﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria.GameContent.UI.Elements;
using Terraria.UI;
using Terraria;

using Microsoft.Xna.Framework;
using ReLogic.Content;
namespace bettergame.UI.Common
{
    public class MovePanel : UIPanel
    {
        private bool _moving = false;

        private Vector2 _mousePosition = Vector2.Zero;

        private Vector2 _originPosition = Vector2.Zero;

        /// <summary>
        /// 是否禁止移动面板
        /// </summary>
        public bool Disabled { get; set; }

        public MovePanel(bool disabled = false, float transparency = 0.7f) : base()
        {
            BackgroundColor = new Color(63, 82, 151) * transparency;
            Disabled = disabled;
        }

        public override void LeftMouseDown(UIMouseEvent evt)
        {
            if (!Disabled && evt.Target == this)
            {
                _originPosition = GetDimensions().Position();
                _mousePosition = new Vector2(Main.mouseX, Main.mouseY);
                _moving = true;
            }
            base.LeftMouseDown(evt);
        }

        public override void LeftMouseUp(UIMouseEvent evt)
        {
            if (!Disabled)
            {
                _moving = false;
            }
            base.LeftMouseUp(evt);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (_moving)
            {
                Vector2 offset = new Vector2(Main.mouseX, Main.mouseY) - _mousePosition;
                Top.Set(_originPosition.Y + offset.Y, 0);
                Left.Set(_originPosition.X + offset.X, 0);
                base.Recalculate();
            }
            base.Draw(spriteBatch);
        }
    }
}
