﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.GameContent.UI.Elements;
using Terraria.UI;

namespace bettergame.UI.Common
{
    public class ScrollPanel(UIElement content, float scrollBarWidth = 20f, bool disabled = false, float transparency = 0.7f) : MovePanel(disabled, transparency)
    {
        protected readonly float _scrollBarWidth = scrollBarWidth;

        protected readonly float _gap = 10f;

        private float _positionRatio;

        private float _sizeRatio;

        protected Scrollbar Scrollbar { get; set; } = new();

        protected UIElement Container { get; set; } = new();

        protected UIElement Content { get; set; } = content;

        public bool ScrollVisible { get; set; } = true;

        public override void OnInitialize()
        {
            // 设置父容器
            Container.Width.Set(Content.Width.Pixels + _gap + _scrollBarWidth + Container.PaddingLeft + Container.PaddingRight + Container.MarginLeft + Container.MarginRight, 0);
            Container.Height.Set(Height.Pixels - PaddingTop - PaddingBottom - Container.MarginTop - Container.MarginBottom, 0);
            Container.OverflowHidden = true;

            // 设置内容区域
            Content.OnUpdate += OnContentUpdate;

            // 添加滚动条
            Scrollbar.Left.Set(Content.Width.Pixels + _gap - 2, 0);
            Scrollbar.Width.Set(_scrollBarWidth, 0);
            Scrollbar.Height.Set(Height.Pixels - PaddingTop - PaddingBottom - Container.MarginTop - Container.MarginBottom, 0);
            Scrollbar.OnViewPositionChanged += ScrollBar_OnViewPositionChanged;

            Container.Append(Content);
            Container.Append(Scrollbar);

            // 设置滚动面板
            Width.Set(Container.Width.Pixels + PaddingLeft + PaddingRight, 0);

            Append(Container);
            base.OnInitialize();
        }

        // 监听滚动条位置变化
        private void ScrollBar_OnViewPositionChanged(float ratio, float value, float maxSize)
        {
            CalculatedStyle dimension = Content.GetDimensions();

            float position = ratio * dimension.Height;

            Content.Top.Set(-position, 0);
        }

        // 绑定内容区域高度变化事件（改变滚动条）
        private void OnContentUpdate(UIElement UIElement)
        {
            if (Content != null)
            {
                CalculatedStyle contentDim = Content.GetDimensions();

                CalculatedStyle innerDim = Container.GetInnerDimensions();

                float contentPosition = Math.Abs(Content.Top.Pixels);

                float postionRatio = contentPosition / contentDim.Height;

                float sizeRatio = innerDim.Height / contentDim.Height;

                if (_sizeRatio != sizeRatio)
                {
                    _sizeRatio = sizeRatio;
                    _positionRatio = postionRatio;
                    Scrollbar.SetView(_positionRatio, _sizeRatio);
                }
                else if (_positionRatio != postionRatio)
                {
                    _positionRatio = postionRatio;
                    Scrollbar.SetView(_positionRatio);
                }
            }
        }

        public override void ScrollWheel(UIScrollWheelEvent evt)
        {
            if (evt != null)
            {
                if (evt.ScrollWheelValue > 0)
                {
                    _positionRatio -= 0.1f;
                    Scrollbar.SetView(_positionRatio);
                }
                else
                {
                    _positionRatio += 0.1f;
                    Scrollbar.SetView(_positionRatio);
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (Scrollbar.Visible != ScrollVisible)
            {
                Scrollbar.Visible = ScrollVisible;
                float width = ScrollVisible ? Width.Pixels + _gap + _scrollBarWidth : Width.Pixels - _gap - _scrollBarWidth;
                Width.Set(width, 0);
                Recalculate();
            }
            base.Draw(spriteBatch);
        }
    }
}
