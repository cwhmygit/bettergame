﻿#nullable enable
using Terraria;
using Terraria.UI;
using Terraria.ID;
using ReLogic.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria.GameContent; //引入TextureASSet类
using Terraria.ModLoader;
using Terraria.Audio;
using Terraria.GameContent.UI.Chat;
using Terraria.UI.Chat;
using bettergame.Utils;
using Terraria.DataStructures;
namespace bettergame.UI.Common
{
    public class SlotUI(Item item, Color? color = null) : UIElement
    {
        public Item Item = item ?? new Item();

        // 渲染颜色
        public Color color = color ?? Color.White;

        // 物品槽贴图
        private Texture2D SlotTexture => Item.favorited ? TextureAssets.InventoryBack10.Value : TextureAssets.InventoryBack9.Value;

        // 获取物品对应的贴图
        private Asset<Texture2D> ItemTexture => TextureAssets.Item[Item.type];

        // 内容区域尺寸
        private Rectangle Content => GetInnerDimensions().ToRectangle();

        // 绘制物品槽
        protected override void DrawSelf(SpriteBatch spriteBatch)
        {
            // 获取容器位置信息x、y、width、height
            CalculatedStyle dimensions = GetDimensions();
            // 背景图片尺寸相对于容器的增缩比例
            float backScale = dimensions.Width / SlotTexture.Width;
            // 绘制物品槽
            spriteBatch.Draw(SlotTexture, dimensions.Position(), SlotTexture.Bounds, color, 0f, Vector2.Zero, backScale, SpriteEffects.None, 0f);

            // 加载物品
            Main.instance.LoadItem(Item.type);
            // 物品是否有动画
            DrawAnimation drawAnimation = Main.itemAnimations[Item.type];
            // 定义物品大小的矩形方框
            Rectangle itemRect = drawAnimation != null ? drawAnimation.GetFrame(ItemTexture.Value) : ItemTexture.Frame(1, 1, 0, 0);

            // 物品尺寸相对于容器的增缩比例
            float itemScale = 1f;
            if (itemRect.Width > Content.Width || itemRect.Height > Content.Width)
            {
                itemScale = (float)Content.Width / (itemRect.Width > itemRect.Height ? (float)itemRect.Width : (float)itemRect.Height);
            }

            // 物品在容器中绘制的坐标
            Vector2 itemDrawPosition = new(Content.X, Content.Y);

            itemDrawPosition.X += ((float)Content.Width - (float)itemRect.Width * itemScale) / 2f;
            itemDrawPosition.Y += ((float)Content.Height - (float)itemRect.Height * itemScale) / 2f;

            // 获取物品颜色
            Color Color = Item.color == Color.Transparent ?  Item.GetAlpha(Color.White) : Item.GetColor(Color.White);
            // 绘制物品
            spriteBatch.Draw(ItemTexture.Value, itemDrawPosition, new Rectangle?(itemRect), Color, 0f, Vector2.Zero, itemScale, SpriteEffects.None, 0f);

            // 绘制物品的数量
            if (Item.stack > 1)
            {
                Terraria.Utils.DrawBorderStringFourWay(spriteBatch, FontAssets.ItemStack.Value, Item.stack.ToString(), Content.X + Content.Width * 0.2f, Content.Y + Content.Height * 0.6f, Color.White, Color.Black, new Vector2(0.3f), 0.75f);
            }

            if (IsMouseHovering) DrawText();
        }


        /// <summary>
        /// 改原版的<see cref="Main.cursorOverride"/>
        /// </summary>
        protected virtual void SetCursorOverride()
        {
            if (!Item.IsAir)
            {
                if (!Item.favorited && ItemSlot.ShiftInUse)
                {
                    Main.cursorOverride = CursorOverrideID.ChestToInventory; // 快捷放回物品栏图标
                }
                if (Main.keyState.IsKeyDown(Main.FavoriteKey))
                {
                    Main.cursorOverride = CursorOverrideID.FavoriteStar; // 收藏图标
                    if (Main.drawingPlayerChat)
                    {
                        Main.cursorOverride = CursorOverrideID.Magnifiers; // 放大镜图标 - 输入到聊天框
                    }
                }
                void TryTrashCursorOverride()
                {
                    if (!Item.favorited)
                    {
                        if (Main.npcShop > 0)
                        {
                            Main.cursorOverride = CursorOverrideID.QuickSell; // 卖出图标
                        }
                        else
                        {
                            Main.cursorOverride = CursorOverrideID.TrashCan; // 垃圾箱图标
                        }
                    }
                }
                if (ItemSlot.ControlInUse && ItemSlot.Options.DisableLeftShiftTrashCan && !ItemSlot.ShiftForcedOn)
                {
                    TryTrashCursorOverride();
                }
            }
        }

        /// <summary>
        /// 鼠标快捷默认行为
        /// </summary>
        /// <returns></returns>
        public virtual bool CursorAction()
        {
            SetCursorOverride();
            if (!Main.LocalPlayer.ItemTimeIsZero || Main.LocalPlayer.itemAnimation != 0)
            {
                return true;
            }
            // 放大镜图标 - 输入到聊天框
            if (Main.cursorOverride == CursorOverrideID.Magnifiers)
            {
                if (ChatManager.AddChatText(FontAssets.MouseText.Value, ItemTagHandler.GenerateTag(Item), Vector2.One))
                    SoundEngine.PlaySound(SoundID.MenuTick);
                return true;
            }

            // 收藏图标
            if (Main.cursorOverride == CursorOverrideID.FavoriteStar)
            {
                Item.favorited = !Item.favorited;
                SoundEngine.PlaySound(SoundID.MenuTick);
                return true;
            }

            // 垃圾箱图标
            if (Main.cursorOverride == CursorOverrideID.TrashCan)
            {
                // 假装自己是一个物品栏物品
                ItemSlot.SellOrTrash([Item], ItemSlot.Context.InventoryItem, 0);
                return true;
            }

            // 放回物品栏图标
            if (Main.cursorOverride == CursorOverrideID.ChestToInventory)
            {
                int oldStack = Item.stack;
                Item = Main.player[Main.myPlayer].GetItem(Main.myPlayer, Item, GetItemSettings.InventoryEntityToPlayerInventorySettings);
                if (Item.stack != oldStack) // 成功了
                {
                    if (Item.stack <= 0)
                        Item.SetDefaults();
                    SoundEngine.PlaySound(SoundID.Grab);
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// 修改MouseText的
        /// </summary>
        public virtual void DrawText()
        {
            if (!Item.IsAir && (Main.mouseItem is null || Main.mouseItem.IsAir))
            {
                Main.HoverItem = Item.Clone();
                Main.instance.MouseText(string.Empty);
            }
        }
    }
}
