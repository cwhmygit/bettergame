﻿#nullable enable
using Terraria;
using Terraria.UI;
using Terraria.ID;
using ReLogic.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria.GameContent; //引入TextureASSet类
using Terraria.ModLoader;
using Terraria.Audio;
using Terraria.GameContent.UI.Chat;
using Terraria.UI.Chat;
using bettergame.Utils;
using Terraria.DataStructures;
using bettergame.Modules.BigPack;

namespace bettergame.UI.Common
{
    public class SlotItemUI(Item item, Color? color = null) : SlotUI(item, color)
    {

        // 开启物品槽右键默认功能（打开宝匣、宝藏袋、切换饰品等）
        protected virtual bool DefaultRightClick => true;

        // 默认的物品鼠标行为
        protected virtual bool DefaultCursorAction => true;

        // 处理右键行为
        public override void Draw(SpriteBatch spriteBatch)
        {
            if (Item.IsAir) Item.SetDefaults();
            if (IsMouseHovering)
            {
                if (DefaultRightClick  ) ItemSlot.RightClick(ref Item, ItemSlot.Context.InventoryItem);
                if (DefaultCursorAction) SetCursorOverride();
            }
            base.Draw(spriteBatch);
        }

        public override void LeftMouseDown(UIMouseEvent evt)
        {
            if (DefaultCursorAction)
            {
                if (CursorAction()) return;
            }

            if (Main.mouseItem.IsAir && Item.IsAir) return;

            byte placeMode = Extension.CanPlaceInSlot(Item, Main.mouseItem);

            if (placeMode is 1 || placeMode is 3)
            {
                // 切换物品
                Extension.SwapItem(ref Item, ref Main.mouseItem);
                SoundEngine.PlaySound(SoundID.Grab);
            }
            else if (placeMode is 2)
            {
                ItemLoader.TryStackItems(Item, Main.mouseItem, out _);
                SoundEngine.PlaySound(SoundID.Grab);
            }
            base.LeftMouseDown(evt);
        }

        public override void LeftMouseUp(UIMouseEvent evt)
        {
            Recipe.FindRecipes();
            base.LeftMouseUp(evt);
        }

        public override void RightMouseUp(UIMouseEvent evt)
        {
            Recipe.FindRecipes();
            base.RightMouseUp(evt);
        }
    }
}
