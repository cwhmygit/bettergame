﻿#nullable enable
using Terraria;
using Terraria.UI;
using Terraria.ID;
using ReLogic.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria.GameContent;
using Terraria.ModLoader;
using Terraria.Audio; //引入TextureASSet类

namespace bettergame.UI.Common
{
    public class ImageButton(Asset<Texture2D> asset, Color color) : UIElement
    {
        private readonly Asset<Texture2D> _asset = asset;

        private readonly float _visibleBright = 1f;

        private readonly float _visibleDark = 0.4f;

        private bool _isHovering = false;

        private Color _color = color;

        public Color Color
        {
            get { return _color; }
            set
            {
                _color = value;
            }
        }

        protected override void DrawSelf(SpriteBatch spriteBatch)
        {
            Texture2D texture = _asset.Value;
            CalculatedStyle dimensions = base.GetDimensions();
            float backScale = 1f;
            // 背景图片尺寸相对于容器的增缩比例
            if(dimensions.Width / dimensions.Height > texture.Width / texture.Height){
                backScale = (float)dimensions.Height / (float)texture.Height;
            }
            else {
                backScale = (float)dimensions.Width / (float)texture.Width;
            }
            // 绘制物品槽
            spriteBatch.Draw(texture, dimensions.Position(), texture.Bounds, _color * (_isHovering ? _visibleBright : _visibleDark), 0f, Vector2.Zero, backScale, SpriteEffects.None, 0f);
        }

        public override void MouseOver(UIMouseEvent evt)
        {
            _isHovering = true;
            base.MouseOver(evt);
        }

        public override void MouseOut(UIMouseEvent evt)
        {
            _isHovering = false;
            base.MouseOut(evt);
        }
    }
}
