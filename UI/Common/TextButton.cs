﻿using Terraria;
using Terraria.UI;
using Microsoft.Xna.Framework;
using Terraria.GameContent; //引入TextureASSet类
using Terraria.GameContent.UI.Elements;
using Microsoft.Xna.Framework.Graphics;
using Terraria.ModLoader;
using Terraria.ModLoader.UI.Elements;
using Terraria.Audio;
using Terraria.ID;
using System.Security.Cryptography;
namespace bettergame.UI.Common
{
    public class TextButton : UIPanel
    {
        private readonly UIText _text;

        public Color TextColor
        {
            get => _text.TextColor;
            set => _text.TextColor = value;
        }

        private bool _sound = false;

        private bool _hover = false;

        private bool _selected = false;

        public bool Hover
        {
            get => _hover;
            set
            {
                if (_hover == value) return;
                _hover = value;
                OnHoverChanged?.Invoke(this, value);
            }
        }

        public bool Selected
        {
            get { return _selected; }
            set
            {
                if (_selected == value) return;
                _selected = value;
                OnSelectChanged?.Invoke(this, value);
            }
        }

        public delegate void HoverChange(TextButton textButton, bool isHover);

        public delegate void SelectChange(TextButton textButton, bool isSelect);

        public event HoverChange OnHoverChanged;

        public event SelectChange OnSelectChanged;

        public TextButton(string text, float textScale = 1f, float paddingLeft = 0f, float paddingTop = 0f, float paddingRight = 0f, float paddingBottom = 0f)
        {
            _text = new(text, textScale);
            _text.Recalculate();

            CalculatedStyle textDim = _text.GetDimensions();

            PaddingLeft = paddingLeft;
            PaddingTop = paddingTop;
            PaddingRight = paddingRight;
            PaddingBottom = paddingBottom;

            Width.Set(textDim.Width + PaddingLeft + PaddingRight, 0);
            Height.Set(textDim.Height + PaddingTop + PaddingBottom, 0);
        }

        public TextButton(string text, float textScale = 1f, float padding = 5f)
        {
            _text = new(text, textScale);
            _text.Recalculate();

            CalculatedStyle textDim = _text.GetDimensions();

            SetPadding(padding);

            Width.Set(textDim.Width + PaddingLeft + PaddingRight, 0);
            Height.Set(textDim.Height + PaddingTop + PaddingBottom, 0);
        }

        public override void OnInitialize()
        {
            Append(_text);
            base.OnInitialize();
        }

        public void ChangeText(string text, Color color, float scale)
        {
            TextColor = color;
            _text.SetText(text, scale, false);
        }

        public override void MouseOver(UIMouseEvent evt)
        {
            Hover = true;
            base.MouseOver(evt);
        }

        public override void MouseOut(UIMouseEvent evt)
        {
            Hover = false;
            base.MouseOut(evt);
        }

        public override void LeftClick(UIMouseEvent evt)
        {
            Selected = !Selected;
            SoundEngine.PlaySound(SoundID.Mech);
            base.LeftClick(evt);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (_hover)
            {
                if (!_sound)
                {
                    SoundEngine.PlaySound(SoundID.Mech);
                    _sound = true;
                }
            }
            else
            {
                _sound = false;
            }
            base.Draw(spriteBatch);
        }
    }
}
