﻿#nullable enable
using Terraria;
using Terraria.UI;
using Terraria.ID;
using ReLogic.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria.GameContent;
using Terraria.ModLoader;
using Terraria.Audio;
namespace bettergame.UI.Common
{
    public class IconUI(Asset<Texture2D> asset, Color color) : UIElement
    {
        private readonly Asset<Texture2D> asset = asset;

        public virtual Color Color { get; set; } = color;

        protected override void DrawSelf(SpriteBatch spriteBatch)
        {
            Texture2D texture = asset.Value;

            CalculatedStyle dimensions = GetDimensions();

            // 背景图片尺寸相对于容器的增缩比例
            float backScale;
            if (texture.Width / texture.Height >= dimensions.Width / dimensions.Height)
            {
                backScale = dimensions.Width / texture.Width;
            }
            else
            {
                backScale = dimensions.Height / texture.Height;
            }
            // 绘制物品槽
            spriteBatch.Draw(texture, dimensions.Position(), texture.Bounds, Color, 0f, Vector2.Zero, backScale, SpriteEffects.None, 0f);
        }
    }
}
