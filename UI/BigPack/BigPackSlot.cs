﻿using bettergame.Modules.BigPack;
using bettergame.UI.Common;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;

namespace bettergame.UI.BigPack
{
    public class BigPackSlot(Item item, int index, Color? color = null) : SlotItemUI(item, color)
    {
        public Item SlotItem
        {
            get
            {
                return Item;
            }
            private set
            {
                if(BigPackPlayer.LocalPlayer.GetItems[index]?.type != value.type)
                {
                    BigPackPlayer.LocalPlayer.GetItems[index] = value;
                }
            }
        }
             
        protected override void DrawSelf(SpriteBatch spriteBatch)
        {
            SlotItem = Item;
            base.DrawSelf(spriteBatch);
        }
    }
}
