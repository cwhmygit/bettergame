﻿using bettergame.Modules.BigPack;
using bettergame.UI.AutoTrash;
using bettergame.UI.Common;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.GameContent.UI.Elements;
using Terraria.UI;

namespace bettergame.UI.BigPack
{
    public class BigPackGrid(UIElement content = null, float scrollBarWidth = 20, bool disabled = false, float transparency = 0.7F) : ScrollPanel(content, scrollBarWidth, disabled, transparency)
    {
        private TextButton FavorBtn;

        private TextButton CommonBtn;

        public override void OnInitialize()
        {
            SetPadding(15f);

            Container.MarginLeft = 70f;
            Container.MarginTop = 170f;

            Content = new UIElement();
            Content.Width.Set(600f, 0);
            Content.Height.Set(700f, 0);
            Content.MaxHeight.Set(700f, 0);

            UIText title = new("大背包", 0.8f, true);

            FavorBtn = new("收藏", paddingLeft: 5f, paddingTop: 65f, paddingRight: 5f, paddingBottom: 60f);
            FavorBtn.Top.Set(Height.Pixels - FavorBtn.Height.Pixels - PaddingTop - PaddingBottom, 0);
            FavorBtn.TextColor = Color.White;
            FavorBtn.BackgroundColor = new Color(63, 82, 200);
            FavorBtn.OnHoverChanged += Btn_OnHoverChanged;
            FavorBtn.OnSelectChanged += Btn_OnSelectChanged;
            FavorBtn.OnLeftClick += Btn_OnLeftClick;

            CommonBtn = new("普通", paddingLeft: 5f, paddingTop: 65f, paddingRight: 5f, paddingBottom: 60f);
            CommonBtn.Top.Set(Height.Pixels - CommonBtn.Height.Pixels - FavorBtn.Height.Pixels - 20f - PaddingTop - PaddingBottom, 0);
            CommonBtn.TextColor = Color.White;
            CommonBtn.BackgroundColor = new Color(63, 82, 200);
            CommonBtn.OnHoverChanged += Btn_OnHoverChanged;
            CommonBtn.OnSelectChanged += Btn_OnSelectChanged;
            CommonBtn.OnLeftClick += Btn_OnLeftClick;

            SwitchButton synthesisBtn = new(duration: 5);
            synthesisBtn.Width.Set(50f, 0);
            synthesisBtn.Height.Set(23f, 0);
            synthesisBtn.Top.Set(50f, 0);
            synthesisBtn.OnCheckedChange += SynthesisBtn_OnCheckedChange;
            UIText synthesisTxt = new("参与合成", 0.9f);
            synthesisTxt.Top.Set(55f, 0);
            synthesisTxt.Left.Set(55f, 0);

            SwitchButton pickUpBtn = new(duration: 5);
            pickUpBtn.Width.Set(50f, 0);
            pickUpBtn.Height.Set(23f, 0);
            pickUpBtn.Top.Set(50f, 0);
            pickUpBtn.Left.Set(160f, 0);
            pickUpBtn.OnCheckedChange += PickUpBtn_OnCheckedChange;
            UIText pickUpTxt = new("自动拾取", 0.9f);
            pickUpTxt.Top.Set(55f, 0);
            pickUpTxt.Left.Set(215f, 0);


            SwitchButton trashBtn = new(duration: 5);
            trashBtn.Width.Set(50f, 0);
            trashBtn.Height.Set(23f, 0);
            trashBtn.Top.Set(50f, 0);
            trashBtn.Left.Set(320f, 0);
            trashBtn.OnCheckedChange += TrashBtn_OnCheckedChange;
            UIText trashTxt = new("垃圾清理");
            trashTxt.Top.Set(55f, 0);
            trashTxt.Left.Set(375f, 0);


            TextButton takeBtn = new("强夺全部", 1.2f, padding: 15f);
            takeBtn.Top.Set(100f, 0);
            takeBtn.BackgroundColor = new Color(63, 82, 200);
            takeBtn.OnHoverChanged += Btn_OnHoverChanged;

            TextButton storedBtn = new("存放全部", 1.2f, padding: 15f);
            storedBtn.Top.Set(100f, 0);
            storedBtn.Left.Set(140f, 0);
            storedBtn.BackgroundColor = new Color(63, 82, 200);
            storedBtn.OnHoverChanged += Btn_OnHoverChanged;


            TextButton inductionBtn = new("归纳收藏", 1.2f, padding: 15f);
            inductionBtn.Top.Set(100f, 0);
            inductionBtn.Left.Set(280f, 0);
            inductionBtn.BackgroundColor = new Color(63, 82, 200);
            inductionBtn.OnHoverChanged += Btn_OnHoverChanged;


            TextButton stackBtn = new("快速堆叠", 1.2f, padding: 15f);
            stackBtn.Top.Set(100f, 0);
            stackBtn.Left.Set(420f, 0);
            stackBtn.BackgroundColor = new Color(63, 82, 200);
            stackBtn.OnHoverChanged += Btn_OnHoverChanged;


            TextButton sortBtn = new("排序", 1.2f, padding: 15f);
            sortBtn.Top.Set(100f, 0);
            sortBtn.Left.Set(560f, 0);
            sortBtn.BackgroundColor = new Color(63, 82, 200);
            sortBtn.OnHoverChanged += Btn_OnHoverChanged;

            Append(title);
            Append(CommonBtn);
            Append(FavorBtn);
            Append(synthesisBtn);
            Append(pickUpBtn);
            Append(trashBtn);
            Append(synthesisTxt);
            Append(pickUpTxt);
            Append(trashTxt);

            Append(takeBtn);
            Append(storedBtn);
            Append(inductionBtn);
            Append(stackBtn);
            Append(sortBtn);

            CommonBtn.Selected = true;

            base.OnInitialize();
        }

        private void TrashBtn_OnCheckedChange(bool isChecked)
        {
            BigPackPlayer.LocalPlayer.CanAutoTrash = isChecked;
        }

        private void PickUpBtn_OnCheckedChange(bool isChecked)
        {
            BigPackPlayer.LocalPlayer.CanPickUp = isChecked;
        }

        private void SynthesisBtn_OnCheckedChange(bool isChecked)
        {
            BigPackPlayer.LocalPlayer.CanFindRecipe = isChecked;
        }

        private void Btn_OnLeftClick(UIMouseEvent evt, UIElement listeningElement)
        {
            if (listeningElement == FavorBtn)
            {
                BigPackPlayer.LocalPlayer.Column = 1;
            }
            else if (listeningElement == CommonBtn)
            {
                BigPackPlayer.LocalPlayer.Column = 0;
            }
        }

        private void Btn_OnSelectChanged(TextButton textButton, bool isSelect)
        {
            if (isSelect)
            {
                if (textButton == FavorBtn)
                {
                    FavorBtn.BorderColor = Color.LightCyan;
                    FavorBtn.TextColor = Color.Yellow;
                    CommonBtn.Selected = false;
                }
                else
                {
                    CommonBtn.BorderColor = Color.LightCyan;
                    CommonBtn.TextColor = Color.Yellow;
                    FavorBtn.Selected = false;
                }
            }
            else
            {
                if (textButton == FavorBtn)
                {
                    FavorBtn.BorderColor = Color.Black;
                    FavorBtn.TextColor = Color.White;
                }
                else
                {
                    CommonBtn.BorderColor = Color.Black;
                    CommonBtn.TextColor = Color.White;
                }
            }
        }

        private void Btn_OnHoverChanged(TextButton textButton, bool isHover)
        {
            if (isHover)
            {
                textButton.BorderColor = Color.LightCyan;
            }
            else if (textButton == FavorBtn && BigPackPlayer.LocalPlayer.Column is 0)
            {
                textButton.BorderColor = Color.Black;
            }
            else if (textButton == CommonBtn && BigPackPlayer.LocalPlayer.Column is 1)
            {
                textButton.BorderColor = Color.Black;
            }
            else if (textButton != FavorBtn && textButton != CommonBtn) textButton.BorderColor = Color.Black;
        }

        public override void OnActivate()
        {
            ReDraw();
            base.OnActivate();
        }

        public void ReDraw()
        {
            Content.RemoveAllChildren();

            Item[] items = BigPackPlayer.LocalPlayer.GetItems;

            int column = 0, row = 0;
            for (int i = 0; i < items.Length; i++)
            {
                BigPackSlot gridSlot = new(items[i], i);
                gridSlot.Width.Set(48f, 0);
                gridSlot.Height.Set(48f, 0);
                gridSlot.Left.Set(column * 58, 0);
                gridSlot.Top.Set(row * 58, 0);
                if (++column > 9)
                {
                    row++;
                    column = 0;
                }
                Content.Append(gridSlot);
            }
            Content.Height.Set(row * 58, 0);
            Content.MaxHeight.Set(row * 58, 0);
        }
    }
}
