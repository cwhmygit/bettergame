﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria.ModLoader;
using Terraria.UI;
using Terraria;

namespace bettergame.UI
{
    public class UISystem : ModSystem
    {
        private UserInterface _interface;

        private string _layerName;

        public static readonly List<string> UILayers = [];

        public virtual UIState UIState { get; set; }

        public virtual bool Visible { get; set; }

        public virtual void LoadUI(string layerName, UIState state)
        {
            _layerName = layerName;
            UILayers.Add(layerName);

            _interface = new UserInterface();
            UIState = state;
            UIState.Activate();
        }

        public virtual void UnloadUI()
        {
            UIState = null;
            _interface = null;
            Visible = false;
        }

        public virtual void ShowUI()
        {
            if (Visible) return;
            Visible = true;
            _interface?.SetState(UIState);
        }

        public virtual void HideUI()
        {
            if (!Visible) return;
            Visible = false;
            _interface?.SetState(null);
        }

        /// <summary>
        /// 应用于更新动画之前
        /// </summary>
        public virtual void BeforeUpdateUI() { }

        /// <summary>
        /// 应用于更新动画之后
        /// </summary>
        public virtual void UpdatedUI() { }

        public override void UpdateUI(GameTime gameTime)
        {
            BeforeUpdateUI();
            if (_interface?.CurrentState == null) return;
            UpdatedUI();
            _interface?.Update(gameTime);
        }

        public override void ModifyInterfaceLayers(List<GameInterfaceLayer> layers)
        {
            if (_interface?.CurrentState == null) return;
            int mouseTextIndex = layers.FindIndex(layer => layer.Name.Equals("Vanilla: Mouse Text"));
            if (mouseTextIndex != -1)
            {
                layers.Insert(mouseTextIndex, new LegacyGameInterfaceLayer(
                     _layerName,
                    delegate
                    {
                        _interface.Draw(Main.spriteBatch, new GameTime());
                        return true;
                    },
                    InterfaceScaleType.UI)
                );
            }
        }

        public override void OnWorldUnload()
        {
            UnloadUI();
            base.OnWorldUnload();
        }

        public override void OnModUnload()
        {
            UnloadUI();
            base.OnModUnload();
        }
    }
}
