using Terraria;
using Microsoft.Xna.Framework;

namespace bettergame.UI
{
    public class UiHelper
    {
        // 判断鼠标是否在矩形上
        public static bool MouseInPanel(Rectangle rect)
        {
            return rect.Intersects(new Rectangle(Main.mouseX, Main.mouseY, 1, 1));
        }
    }
}